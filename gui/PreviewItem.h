#ifndef PREVIEWITEM_H
#define PREVIEWITEM_H

#include <QtQuick/QQuickFramebufferObject>
#include <QTimer>

class PreviewRenderer;
class PreviewItem : public QQuickFramebufferObject
{
public:
    PreviewItem(QQuickItem *parent = nullptr);

    Renderer *createRenderer() const override;

public slots:
    void on_timeout();

private:
    Q_DISABLE_COPY(PreviewItem)
    mutable PreviewRenderer *m_renderer = nullptr;
    QTimer  *m_timer = nullptr;
};

#endif // PREVIEWITEM_H
