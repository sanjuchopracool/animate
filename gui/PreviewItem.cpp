#include "PreviewItem.h"
#include <PreviewRenderer.h>

PreviewItem::PreviewItem(QQuickItem *parent)
    : QQuickFramebufferObject (parent)
{
    setMirrorVertically(true);
    m_timer = new QTimer(this);
    connect(m_timer, &QTimer::timeout,
            this, &PreviewItem::on_timeout);
    m_timer->start(20);
}

QQuickFramebufferObject::Renderer *PreviewItem::createRenderer() const
{
    m_renderer = new PreviewRenderer;
    return m_renderer;
}

void PreviewItem::on_timeout()
{
    m_renderer->step();
}
