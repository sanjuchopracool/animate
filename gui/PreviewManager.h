#ifndef PREVIEWMANAGER_H
#define PREVIEWMANAGER_H

#include <QObject>
#include <QQmlEngine>
#include <common.h>

class PreviewManager : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int parent_height READ parent_height WRITE set_parent_height)
    Q_PROPERTY(int parent_width READ parent_width WRITE set_parent_width)
    Q_PROPERTY(int height READ height NOTIFY height_changed)
    Q_PROPERTY(int width READ width NOTIFY width_changed)

public:

    int parent_height() const { return m_parent_height; }
    void set_parent_height(int height);

    int parent_width() const { return m_parent_width; }
    void set_parent_width(int width);

    int width() const { return m_width; }
    int height() const { return m_height; }

    static void register_types() {
        qmlRegisterSingletonType<PreviewManager>(package_name_chops, 1, 0, "PreviewManager",
                                 [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject *{
            Q_UNUSED(engine)
            Q_UNUSED(scriptEngine)
            if(!m_preview_manager)
                m_preview_manager = new PreviewManager;

            return m_preview_manager;

        });
    }

signals:
    void width_changed();
    void height_changed();

private:
    explicit PreviewManager(QObject *parent = nullptr);

private slots:
    void reevaluate_size();

private:
    static PreviewManager *m_preview_manager;
    int m_parent_width = 0;
    int m_parent_height = 0;
    int m_width = 100;
    int m_height = 100;
};

#endif // PREVIEWMANAGER_H
