#include "PreviewManager.h"
#include <ExportSetting.h>

PreviewManager *PreviewManager::m_preview_manager = nullptr;


void PreviewManager::set_parent_height(int height)
{
    if (m_parent_height != height)
    {
        m_parent_height = height;
        reevaluate_size();
    }
}

void PreviewManager::set_parent_width(int width)
{
    if (m_parent_width != width)
    {
        m_parent_width = width;
        reevaluate_size();
    }
}

PreviewManager::PreviewManager(QObject *parent) : QObject(parent)
{
    connect( ExportSetting::instance(), SIGNAL(current_resolution_changed()),
             this, SLOT(reevaluate_size()));

    connect( ExportSetting::instance(), SIGNAL(picture_mode_changed()),
             this, SLOT(reevaluate_size()));

}

void PreviewManager::reevaluate_size()
{
    ExportSetting* export_setting = ExportSetting::instance();
    auto orientation = export_setting->picture_mode();

    int width_for_height  = 0;
    int height_of_width = 0;

    if (ExportSetting::Square == orientation)
    {
        width_for_height = height_of_width = qMin(m_parent_height, m_parent_width);
    }
    else
    {
        Resolution width_height_ratio = orientation == ExportSetting::Landscape ?
                    export_setting->current_resolution() :
                    export_setting->current_resolution().inverse();

        // height based calculation
        width_for_height = (m_parent_height*width_height_ratio.m_width)/width_height_ratio.m_height;

        if (width_for_height > m_parent_width)
        {
            //calculation must be based on width
            height_of_width = (m_parent_width*width_height_ratio.m_height)/width_height_ratio.m_width;
            width_for_height = m_parent_width;
        }
        else
        {
            height_of_width = m_parent_height;
        }
    }

    if (m_width != width_for_height)
    {
        m_width = width_for_height;
        emit width_changed();
    }

    if (m_height != height_of_width)
    {
        m_height = height_of_width;
        height_changed();
    }
}
