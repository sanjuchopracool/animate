#include "PreviewRenderer.h"

#include <QOpenGLFramebufferObjectFormat>
#include <StripeBackground.h>

#include <QOpenGLPaintDevice>
#include <QElapsedTimer>
#include <QPainter>
#include <ColorSetModel.h>
#include <BackgroundNodeFactory.h>

class Profiler : public QElapsedTimer
{
public:
    Profiler(QString name) : m_name(name) {
        this->start();
    }

    ~Profiler() {
        qDebug() << m_name << " " << elapsed();
    }
    private:
    QString m_name;
};

PreviewRenderer::PreviewRenderer(QObject *parent) :
    QObject(parent)
{
    on_background_node_changed(BackgroundNodeFactory::Margin);
    m_timeline.setDefaultRemoveOnFinish(false);
    m_timeline.setFinishFn( [=] ()
    {
        qDebug() << "Finished";
        this->m_timeline.resetTime();
    });

    m_root_node->update_node_animations(&m_timeline);
    connect(ColorSetModel::instance(), SIGNAL(background_set_changed()),
            this, SLOT(on_background_color_set_changed()));
    connect(BackgroundNodeFactory::instance(), SIGNAL(background_style_changed(int)),
            this, SLOT(on_background_node_changed(int)));
}

PreviewRenderer::~PreviewRenderer()
{
    if (m_root_node)
        delete m_root_node;
}

void PreviewRenderer::synchronize(QQuickFramebufferObject *obj)
{
    m_window = obj->window();
    if (m_reset_fbo)
    {
        m_reset_fbo = false;
        invalidateFramebufferObject();
    }
}

void PreviewRenderer::render()
{
//        Profiler pf1("Total");
    QOpenGLPaintDevice device(framebufferObject()->size());
    {
//            Profiler pro("render");
        QPainter painter(&device);
        painter.setRenderHint(QPainter::Antialiasing);

        if (m_size_changed)
        {
            m_root_node->canvas_size_changed(framebufferObject()->size());
            m_size_changed = false;
        }

        m_root_node->update_node();
        m_root_node->render_node(&painter);
    }

    if(m_dirty)
    {
        m_dirty = false;
        update();
        m_window->resetOpenGLState();
    }
}

void PreviewRenderer::step()
{
    m_timeline.step(1.0/50.0);
    update();
}

QOpenGLFramebufferObject *PreviewRenderer::createFramebufferObject(const QSize &size)
{
    m_dirty = true;
    m_size_changed = true;
    QOpenGLFramebufferObjectFormat format;
    format.setSamples(m_sample_count);
    format.setAttachment(QOpenGLFramebufferObject::CombinedDepthStencil);
    return new QOpenGLFramebufferObject(size, format);
}

void PreviewRenderer::on_background_color_set_changed()
{
    if (m_root_node)
    {
        m_root_node->update_background_color();
    }
}

void PreviewRenderer::on_background_node_changed(int index)
{
    if (m_root_node && index < BackgroundNodeFactory::LastBackground)
    {
        BackgroundNodeFactory::BackgroundNodeType node_type =
                (BackgroundNodeFactory::BackgroundNodeType)index;
        IBackgroundNode *node = BackgroundNodeFactory::create(node_type);
        node->update_node_animations(&m_timeline);

        m_root_node->add_background(node);
        m_timeline.resetTime();
    }
}
