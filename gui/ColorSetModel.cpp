#include "ColorSetModel.h"

#include <QQmlEngine>
#include <common.h>

namespace {
ColorSetModel* m_model = nullptr;
ColorSet m_defalut_color_set;
}

ColorSetModel::ColorSetModel(QObject *parent)
    : QAbstractListModel(parent)
{
    m_color_sets << ColorSet();
    ColorSet set;
    set.m_color1 = 0x2d3436;
    set.m_color2 = 0x636e72;
    set.m_color3 = 0x7f8c8d;
    set.m_color4 = 0x2ecc71;
    m_color_sets << set;

    set.m_color1 = 0xff7979;
    set.m_color2 = 0x130f40;
    set.m_color3 = 0xdff9fb;
    set.m_color4 = 0xe056fd;
    m_color_sets << set;

    set.m_color1 = 0xc8d6e5;
    set.m_color2 = 0x10ac84;
    set.m_color3 = 0xfeca57;
    set.m_color4 = 0x341f97;
    m_color_sets << set;

    set.m_color1 = 0x00a8ff;
    set.m_color2 = 0xe84118;
    set.m_color3 = 0xf5f6fa;
    set.m_color4 = 0x353b48;
    m_color_sets << set;
}

void ColorSetModel::register_types()
{
    qmlRegisterUncreatableType<ColorSet>(package_name_chops,
                              1, 0,
                              "ColorSet",
                              "cannot create ColorSet from QML");

    qmlRegisterSingletonType<ColorSetModel>(package_name_chops, 1, 0, "ColorSetModel",
                             [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject *{
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        return instance();
    });
}

int ColorSetModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_color_sets.count();
}

QVariant ColorSetModel::data(const QModelIndex &index, int role) const
{
    QColor result;
    int row = index.row();
    if (row >= 0 && row <m_color_sets.count())
    {
        const ColorSet& set = m_color_sets[row];
        switch (role) {
        case Color1:
            result = set.m_color1;
            break;
        case Color2:
            result = set.m_color2;
            break;
        case Color3:
            result = set.m_color3;
            break;
        case Color4:
            result = set.m_color4;
            break;
        default:
            break;
        }
    }

    return result;
}

QHash<int, QByteArray> ColorSetModel::roleNames() const
{
    QHash<int, QByteArray> roleNames;
    roleNames[Color1] = "color1";
    roleNames[Color2] = "color2";
    roleNames[Color3] = "color3";
    roleNames[Color4] = "color4";
    return roleNames;
}

ColorSetModel *ColorSetModel::instance()
{
    if (not m_model)
    {
        m_model = new ColorSetModel;
    }
    return m_model;
}

const ColorSet &ColorSetModel::colorSet(int index) const
{
    if(index >= 0 && index < m_color_sets.count())
        return m_color_sets[index];
    return m_defalut_color_set;
}

void ColorSetModel::set_background_set_index(int i)
{
    if (m_background_index != i)
    {
        m_background_index = i;
        emit background_set_changed();
    }
}

const ColorSet &ColorSetModel::background_color_set() const
{
    if (m_color_sets.count() && m_background_index < m_color_sets.count())
        return m_color_sets[m_background_index];

    return m_defalut_color_set;
}

