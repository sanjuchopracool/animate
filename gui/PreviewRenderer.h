#ifndef PREVIEWRENDERER_H
#define PREVIEWRENDERER_H

#include <QObject>
#include <QQuickWindow>
#include <QQuickFramebufferObject>

#include <RootNode.h>
#include <Timeline.h>

class RootNode;
class PreviewRenderer : public QObject, public QQuickFramebufferObject::Renderer
{
    Q_OBJECT
public:
    explicit PreviewRenderer(QObject *parent = nullptr);
    ~PreviewRenderer();

    void synchronize(QQuickFramebufferObject *obj) override;
    void render() override;
    void step();

    QOpenGLFramebufferObject *createFramebufferObject(const QSize &size) override;

private slots:
    void on_background_color_set_changed();
    void on_background_node_changed(int index);

private:
    int m_sample_count = 16;
    bool m_reset_fbo = false;
    QQuickWindow* m_window = nullptr;
    bool m_dirty = true;
    RootNode* m_root_node = new RootNode;
    bool m_size_changed = false;
    choreograph::Timeline m_timeline;
};

#endif // PREVIEWRENDERER_H
