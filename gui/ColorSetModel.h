#ifndef COLORSETMODEL_H
#define COLORSETMODEL_H

#include <QAbstractListModel>
#include <ColorSet.h>

class ColorSetModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int background_set_index READ background_set_index WRITE set_background_set_index NOTIFY background_set_changed)
    enum ColorSetRole
    {
        Color1 = Qt::UserRole + 1,
        Color2,
        Color3,
        Color4
    };

public:
    explicit ColorSetModel(QObject *parent = nullptr);
    static void register_types();

    int rowCount(const QModelIndex &parent) const override;

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    static ColorSetModel *instance();

    Q_INVOKABLE void set_background_set_index(int i);
    int background_set_index() const {
        return m_background_index;
    }

    const ColorSet &background_color_set() const;
signals:
    void background_set_changed();

private:
    const ColorSet &colorSet(int index) const;

private:
    QList<ColorSet> m_color_sets;
    int m_background_index = 0;
};

#endif // COLORSETMODEL_H
