INCLUDEPATH +=$$PWD

SOURCES += \
    $$PWD/PreviewItem.cpp \
    $$PWD/PreviewManager.cpp \
    $$PWD/ColorSetModel.cpp \
    $$PWD/PreviewRenderer.cpp

HEADERS += \
    $$PWD/PreviewItem.h \
    $$PWD/PreviewManager.h \
    $$PWD/ColorSetModel.h \
    $$PWD/PreviewRenderer.h
