#include "ExportSetting.h"
#include <QQmlEngine>
#include <common.h>

ExportSetting* ExportSetting::m_instance = nullptr;

void ExportSetting::set_current_resolution(const Resolution &resolution)
{
    if ( m_current_resolution != resolution)
    {
        m_current_resolution = resolution;
        emit current_resolution_changed();
    }
}

void ExportSetting::set_picture_mode(ExportSetting::PictureMode picture_mode)
{
    if ( m_picture_mode != picture_mode )
    {
        m_picture_mode = picture_mode;
        emit picture_mode_changed();
    }
}

ExportSetting *ExportSetting::instance()
{
    if (!m_instance)
    {
        m_instance = new ExportSetting;
    }

    return m_instance;
}

void ExportSetting::register_types()
{
    qmlRegisterUncreatableType<Resolution>(package_name_chops, 1, 0, "Resolution",
                                            "can not create Resolution type object from qml");

    qmlRegisterSingletonType<ExportSetting>(package_name_chops, 1, 0, "ExportSetting",
                             [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject *{
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        return instance();
    });
}

ExportSetting::ExportSetting(QObject *parent) : QObject(parent)
{
    Resolution resolution;
    resolution.m_width = 1280;
    resolution.m_height = 720;
    resolution.name_ = "720p";
    m_supported_resolutions.reserve(3);
    m_supported_resolutions.append(resolution);

    resolution.m_width = 1920;
    resolution.m_height = 1080;
    resolution.name_ = "1080p";
    m_supported_resolutions.append(resolution);

    resolution.m_width = 3840;
    resolution.m_height = 2160;
    resolution.name_ = "4k";
    m_supported_resolutions.append(resolution);
}
