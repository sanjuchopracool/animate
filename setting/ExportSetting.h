#ifndef EXPORT_SETTING_H
#define EXPORT_SETTING_H

#include <QObject>

struct Resolution
{
    Q_GADGET
    Q_PROPERTY(int width MEMBER m_width)
    Q_PROPERTY(int height MEMBER m_height)
    Q_PROPERTY(QString name MEMBER name_)

public:
    bool operator ==(const Resolution& other) {
        return m_width == other.m_width &&
                m_height == other.m_height;
    }

    bool operator !=(const Resolution& other) {
        return m_width != other.m_width ||
                m_height != other.m_height;
    }

    Resolution inverse() const {
        Resolution reversed;
        reversed.m_width = m_height;
        reversed.m_height = m_width;
        return reversed;
    }

    int m_width = 1920;
    int m_height = 1080;
    QString name_;
};

Q_DECLARE_METATYPE(Resolution)

class ExportSetting : public QObject
{
    Q_OBJECT
    Q_PROPERTY(Resolution current_resolution READ current_resolution
               WRITE set_current_resolution NOTIFY current_resolution_changed )

    Q_PROPERTY(PictureMode picture_mode READ picture_mode
               WRITE set_picture_mode NOTIFY picture_mode_changed)
public:

    enum PictureMode {
        Landscape,
        Portrait,
        Square
    };
    Q_ENUMS(PictureMode)

    const Resolution &current_resolution() const { return m_current_resolution; }
    void set_current_resolution(const Resolution &resolution);

    PictureMode picture_mode() const { return m_picture_mode; }
    void set_picture_mode(PictureMode picture_mode);


    static ExportSetting *instance();
    static void register_types();

protected:
    ExportSetting(QObject* parent = nullptr);

signals:
    void current_resolution_changed();
    void picture_mode_changed();

private:
    Resolution m_current_resolution;
    PictureMode m_picture_mode = Landscape;
    QList<Resolution> m_supported_resolutions;
    static ExportSetting *m_instance;
};

#endif // EXPORT_SETTING_H
