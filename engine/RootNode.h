#ifndef ROOTNODE_H
#define ROOTNODE_H

#include <INode.h>
#include <IBackGroundNode.h>

class RootNode : public INode
{
public:
    RootNode(INode *parent = nullptr);
    void update_background_color();

    void add_background(IBackgroundNode *node);

protected:
    void render(QPainter *painter) override;
    void update_canvas_size(const QSize &size) override;
    void update_animation(choreograph::Timeline *timeline) override;

private:
    IBackgroundNode* m_bg_node = nullptr;
};

#endif // ROOTNODE_H
