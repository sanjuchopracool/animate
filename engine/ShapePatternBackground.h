#ifndef SHAPEPATTERBACKGROUND_H
#define SHAPEPATTERBACKGROUND_H

#include <IBackGroundNode.h>
#include <Output.hpp>

class ShapePatternBackground : public IBackgroundNode
{
public:
    enum ParticleType {
        SolidCircle,
        Circle,
        BrickStyle,
        Triangle,
        Rectangle
    };

    ShapePatternBackground(ParticleType type, INode* parent = nullptr);

    void update();
    void render(QPainter *painter);
    void update_canvas_size(const QSize &size);
    void update_animation(choreograph::Timeline *timeline);

    void set_particle_type(ParticleType type) {
        m_particle_style = type;
        switch (m_particle_style) {
        case SolidCircle:
//            m_particle_dim_factor.m_factor = 0.04;
            break;
        case Triangle:
            m_particle_dim_factor.m_factor = 0.08;
            m_rotation_offset = 5;
            break;
        case Rectangle:
//            m_particle_dim_factor.m_factor = 0.035;
            m_rotation_offset = 5;
            break;
        default:
            break;
        }
    }

private:
    uint_fast8_t m_items_in_smaller_dimension = 7;

    double m_particle_dimension = 0.0;
    DoubleNodeFactor m_particle_dim_factor;

    uint_fast32_t m_column_count = 0;
    uint_fast32_t m_row_count = 0;
    bool m_canvas_size_changed = true;
    double m_rotation_offset = 0.0;

    choreograph::Output<double> m_particle_offset_factor = 0.0;
    choreograph::Output<double> m_circle_radius_factor = 0.0;
    choreograph::Output<double> m_rotation_angle = 0.0;
    ParticleType m_particle_style = SolidCircle;
};

#endif // SHAPEPATTERBACKGROUND_H
