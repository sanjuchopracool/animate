#include "SolidBackground.h"
#include <QPainter>

#include <Timeline.h>
#include <Phrase.hpp>

SolidBackground::SolidBackground(Style style, INode *parent)
    : IBackgroundNode(parent),
      m_margin_factor(m_margin, IntNodeFactor::Min_width_height, 0),
      m_style(style)
{

}

void SolidBackground::render(QPainter *painter)
{
    painter->setPen(QPen(Qt::NoPen));
    if (Margin == m_style)
    {
        painter->setBrush(m_color_set.m_color2);
        painter->drawRect(0, 0, m_width, m_height);
    }
    if (Margin == m_style ||
        Solid == m_style)
    {
        painter->setBrush(m_color_set.m_color1);
        painter->translate(m_margin, m_margin);
        painter->drawRoundedRect(QRect(0, 0, m_width - 2*m_margin, m_height -2*m_margin),
                                 m_margin, m_margin);
    }

    if(Gradient2Color == m_style ||
       GradientAllColor == m_style)
    {
        QLinearGradient gradient(QPointF(0, 0), QPointF(0, m_height));
        if(Gradient2Color == m_style)
        {
            gradient.setColorAt(0, m_color_set.m_color1);
            gradient.setColorAt(1, m_color_set.m_color2);
        }
        else
        {
            gradient.setColorAt(0, m_color_set.m_color1);
            gradient.setColorAt(0.33, m_color_set.m_color2);
            gradient.setColorAt(0.66, m_color_set.m_color3);
            gradient.setColorAt(1, m_color_set.m_color4);
        }

        painter->fillRect(QRect(0, 0, m_width, m_height), gradient);
    }
}

#include <QDebug>
void SolidBackground::update()
{
    INode::update();
    m_margin_factor.update_value(this);
}

void SolidBackground::update_animation(choreograph::Timeline *timeline)
{
    // decrease
    // halt
    // increase
    if (Margin == m_style)
    {
        using namespace choreograph;
        timeline->apply(&m_margin_factor.m_factor).
                then<RampTo>(0.05, 2).
                then<Hold>(0.05, 2).
                then<RampTo>(0.0, 2);
    }
    /*.
            finishFn( [&m = *m_margin_factor.m_factor.inputPtr()] {
              m.resetTime();
            } );*/
}
