#include <INode.h>

#include <QPainter>

INode::~INode()
{
    // delete children first
    for(const auto child : m_children)
    {
        delete child;
    }
}

INode::INode(INode *parent)
    : m_parent(parent),
      m_width_factor(m_width, IntNodeFactor::Parent_width, 1.0),
      m_height_factor(m_height, IntNodeFactor::Parent_height, 1.0)
{

}

void INode::update_node()
{
    update();
    for(auto child : m_children)
    {
        child->update_node();
    }
}

void INode::render_node(QPainter *painter)
{
    painter->save();
    render(painter);
    for(auto child : m_children)
        child->render_node(painter);
    painter->restore();
}

void INode::update_node_animations(choreograph::Timeline *timeline)
{
    update_animation(timeline);
    for(auto child : m_children)
        child->update_node_animations(timeline);
}

void INode::canvas_size_changed(const QSize &size)
{
    update_canvas_size(size);
    for(auto child : m_children)
        child->update_canvas_size(size);
}

void INode::add_child(INode *child)
{
    m_children.append(child);
    child->m_parent = this;
}

void INode::replace_child(INode *to_replace, INode *replace_with)
{
    if (replace_with)
    {
        if (to_replace && m_children.contains(to_replace) )
        {
            replace_with->m_children = to_replace->m_children;
            to_replace->m_children.clear();
            m_children.removeAll(to_replace);
            delete to_replace;
        }
        add_child(replace_with);
    }
}

void INode::update()
{
    m_width_factor.update_value(this);
    m_height_factor.update_value(this);
}
