#include "BackgroundNodeFactory.h"

#include <QQmlEngine>
#include <common.h>

#include <SolidBackground.h>
#include <StripeBackground.h>
#include <ShapePatternBackground.h>

namespace {
BackgroundNodeFactory* m_model = nullptr;
QMap<BackgroundNodeFactory::BackgroundNodeType, QString> name_map =
{
    {BackgroundNodeFactory::Stripe, "Stripe"},
    {BackgroundNodeFactory::SolidCirclePattern, "Filled circle"},
    {BackgroundNodeFactory::CirclePattern, "Circle"},
    {BackgroundNodeFactory::BrickPattern, "Brick"},
    {BackgroundNodeFactory::Margin, "Margin"},
    {BackgroundNodeFactory::Solid, "Solid"},
    {BackgroundNodeFactory::Gradient, "Gradient"},
    {BackgroundNodeFactory::GradientAllColor, "GradientAllColor"},
    {BackgroundNodeFactory::TrianglePattern, "Triangle"},
    {BackgroundNodeFactory::RectanglePattern, "Rectangle"},
};
}

BackgroundNodeFactory::BackgroundNodeFactory(QObject *parent)
    : QAbstractListModel(parent)
{

}

QVariant BackgroundNodeFactory::data(const QModelIndex &index, int role) const
{
    QString result;
    int row = index.row();
    if(row >= 0 && row < No_of_background)
    {
        if (Qt::DisplayRole == role)
        {
            return name_map[BackgroundNodeType(row)];
        }
    }
    return result;
}

QHash<int, QByteArray> BackgroundNodeFactory::roleNames() const
{
    return QAbstractListModel::roleNames();
}

void BackgroundNodeFactory::register_types()
{
    qmlRegisterSingletonType<BackgroundNodeFactory>(package_name_chops, 1, 0, "BackgroundNodeModel",
                             [](QQmlEngine *engine, QJSEngine *scriptEngine) -> QObject *{
        Q_UNUSED(engine)
        Q_UNUSED(scriptEngine)
        return instance();
    });
}

BackgroundNodeFactory *BackgroundNodeFactory::instance()
{
    if (not m_model)
    {
        m_model = new BackgroundNodeFactory;
    }
    return m_model;
}

IBackgroundNode *BackgroundNodeFactory::create(BackgroundNodeFactory::BackgroundNodeType node_type)
{
    IBackgroundNode *result = nullptr;
    switch (node_type) {
    case Margin:
        result = new SolidBackground(SolidBackground::Margin);
        break;
    case Solid:
        result = new SolidBackground(SolidBackground::Solid);
        break;
    case Gradient:
        result = new SolidBackground(SolidBackground::Gradient2Color);
        break;
    case GradientAllColor:
        result = new SolidBackground(SolidBackground::GradientAllColor);
        break;
    case Stripe:
        result = new StripeBackground;
        break;
    case SolidCirclePattern:
        result = new ShapePatternBackground(ShapePatternBackground::SolidCircle);
        break;
    case CirclePattern:
        result = new ShapePatternBackground(ShapePatternBackground::Circle);
        break;
    case BrickPattern:
        result = new ShapePatternBackground(ShapePatternBackground::BrickStyle);
        break;
    case TrianglePattern:
        result = new ShapePatternBackground(ShapePatternBackground::Triangle);
        break;
    case RectanglePattern:
        result = new ShapePatternBackground(ShapePatternBackground::Rectangle);
        break;
    default:
        break;
    }

    return result;
}
