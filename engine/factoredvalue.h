#ifndef FACTOREDVALUE_H
#define FACTOREDVALUE_H

#include <INode.h>
#include <Motion.hpp>
#include <Output.hpp>

template <typename T,
          typename AnimationOutput,
          typename NodeType>
class FactoredValue
{
public:
    enum RelationWith
    {
        Parent_width,
        Parent_height,
        Max_parent_width_height,
        Min_parent_width_height,
        Average_parent_width_height,
        Width,
        Height,
        Max_width_height,
        Min_width_height,
        Average_width_height,
    };

    FactoredValue(T &value, RelationWith relation_with, const AnimationOutput &factor)
        : m_value(value),
          m_relation(relation_with),
          m_factor(factor) {

    }

    const T &update_value(NodeType *node) const {
        NodeType* parent = node->parent_node();
        // Except root node every node will have parent
        // for root node, nothing should changes
        if ( parent)
        {
            T reference_value;
            switch (m_relation) {
            case Parent_width:
                reference_value = parent->width();
                break;
            case Parent_height:
                reference_value = parent->height();
                break;
            case Max_parent_width_height:
                reference_value = qMax(parent->height() , parent->width());
                break;
            case Min_parent_width_height:
                reference_value = qMin(parent->height() , parent->width());
                break;
            case Average_parent_width_height:
                reference_value = (parent->height() + parent->width())/2;
            case Width:
                reference_value = node->width();
                break;
            case Height:
                reference_value = node->height();
                break;
            case Max_width_height:
                reference_value = qMax(node->height() , node->width());
                break;
            case Min_width_height:
                reference_value = qMin(node->height() , node->width());
                break;
            case Average_width_height:
                reference_value = (node->height() + node->width())/2;
            default:
                break;
            }

            m_value = (reference_value*m_factor.value());
        }
        return m_value;
    }

    T &m_value;
    RelationWith m_relation = Parent_width;
    choreograph::Output<AnimationOutput> m_factor;
};
#endif // FACTOREDVALUE_H
