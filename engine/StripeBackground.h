#ifndef STRIPEBACKGROUND_H
#define STRIPEBACKGROUND_H

#include <IBackGroundNode.h>
#include <QColor>

class StripeBackground : public IBackgroundNode
{
public:
    StripeBackground(INode* parent = nullptr);

protected:
    void update() override;
    void render(QPainter *painter) override;
    void update_canvas_size(const QSize &size) override;
    void update_animation(choreograph::Timeline *timeline) override;

private:

private:
    uint_fast32_t m_spacing;
    uint_fast32_t m_stripe_width;
    uint_fast32_t m_offset;
    int_fast32_t m_angle = 30;

    IntNodeFactor m_spacing_factor;
    IntNodeFactor m_stripe_width_factor;
    IntNodeFactor m_offset_factor;

    bool m_update_no_of_stripes = true;
    uint_fast32_t m_no_of_stripes;
};

#endif // STRIPEBACKGROUND_H
