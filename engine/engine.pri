INCLUDEPATH +=$$PWD

SOURCES += \
    $$PWD/INode.cpp \
    $$PWD/RootNode.cpp \
    $$PWD/SolidBackground.cpp \
    $$PWD/StripeBackground.cpp \
    $$PWD/IBackGroundNode.cpp \
    $$PWD/BackgroundNodeFactory.cpp \
    $$PWD/ShapePatternBackground.cpp \

HEADERS += \
    $$PWD/INode.h \
    $$PWD/RootNode.h \
    $$PWD/SolidBackground.h \
    $$PWD/FactoredValue.h \
    $$PWD/StripeBackground.h \
    $$PWD/ColorSet.h \
    $$PWD/IBackGroundNode.h \
    $$PWD/BackgroundNodeFactory.h \
    $$PWD/ShapePatternBackground.h \
