#include "StripeBackground.h"

#include <QPainter>
#include <Timeline.h>
#include <Phrase.hpp>

#define _USE_MATH_DEFINES
#include <math.h>

namespace
{
    inline double degree_to_radian(double angle)
    {
        return (angle*M_PI)/180;
    }
}

StripeBackground::StripeBackground(INode *parent)
    : IBackgroundNode(parent),
      m_spacing_factor(m_spacing, IntNodeFactor::Min_width_height, 0.2),
      m_stripe_width_factor(m_stripe_width, IntNodeFactor::Min_width_height, 0.00),
      m_offset_factor(m_offset, IntNodeFactor::Min_width_height, 0.0)
{
//    m_height_factor.m_factor = 0.9;
}

void StripeBackground::update()
{
    INode::update();
    m_spacing_factor.update_value(this);
    m_stripe_width_factor.update_value(this);
    m_offset_factor.update_value(this);

    // calculate no of stripes if required
    if (m_update_no_of_stripes)
    {
        m_no_of_stripes = (m_width + m_height*std::tan(degree_to_radian(m_angle)))/m_spacing + 1;
        m_update_no_of_stripes = false;
    }
}

void StripeBackground::render(QPainter *painter)
{
    // Draw background
    painter->setPen(QPen(Qt::NoPen));
    painter->setBrush(m_color_set.m_color1);
    painter->drawRect(QRect(0, 0, m_width, m_height));

    // Draw stripes
    painter->setBrush(m_color_set.m_color2);
    int current_offset = m_offset%m_spacing;
    double angle_in_radian = degree_to_radian(m_angle);
    double cos_value = std::cos(angle_in_radian);
    double calculated_corner_height = m_stripe_width*std::tan(angle_in_radian);
    double calculated_corner_hypotenuse = m_stripe_width/cos_value;
    double calculate_height = m_height/cos_value + calculated_corner_height;
    for( uint_fast32_t i = 1; i <= m_no_of_stripes; ++i)
    {
        painter->save();
        painter->translate(i*m_spacing - current_offset - calculated_corner_hypotenuse, 0);
        painter->rotate(m_angle);
        painter->translate(0, -1*calculated_corner_height);
        painter->drawRect(QRectF(0, 0, m_stripe_width, calculate_height));
        painter->restore();
    }
}

void StripeBackground::update_canvas_size(const QSize &size)
{
    Q_UNUSED(size)
    m_update_no_of_stripes = true;
}

void StripeBackground::update_animation(choreograph::Timeline *timeline)
{
    using namespace choreograph;
    timeline->apply(&m_offset_factor.m_factor).
            then<RampTo>(0.6, 6.0);

    timeline->apply(&m_stripe_width_factor.m_factor).
            then<RampTo>(0.032, 1.5).
            then<Hold>(0.032, 3.0).
            then<RampTo>(0.0, 1.5);
}
