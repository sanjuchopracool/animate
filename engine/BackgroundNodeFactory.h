#ifndef BACKGROUNDNODEFACTORY_H
#define BACKGROUNDNODEFACTORY_H

#include <QAbstractListModel>

class IBackgroundNode;
class BackgroundNodeFactory : public QAbstractListModel
{
    Q_OBJECT

public:
    enum BackgroundNodeType
    {
        Stripe,
        SolidCirclePattern,
        CirclePattern,
        BrickPattern,
        TrianglePattern,
        RectanglePattern,
        Margin,
        Solid,
        Gradient,
        GradientAllColor,
        LastBackground,
        No_of_background = LastBackground,
    };

signals:
    void background_style_changed(int index);

public:
    BackgroundNodeFactory(QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const override {
        Q_UNUSED(parent)
        return No_of_background;
    }

    QVariant data(const QModelIndex &index, int role) const override;
    QHash<int, QByteArray> roleNames() const override;

    static void register_types();
    static BackgroundNodeFactory *instance();
    static IBackgroundNode * create(BackgroundNodeType node_type);
};

#endif // BACKGROUNDNODEFACTORY_H
