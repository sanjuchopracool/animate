#ifndef IBACKGROUNDNODE_H
#define IBACKGROUNDNODE_H

#include <INode.h>
#include <ColorSet.h>

class IBackgroundNode : public INode
{
public:
    IBackgroundNode(INode* parent = nullptr);
    void update_color_set();

protected:
    ColorSet m_color_set;
};

#endif // IBACKGROUNDNODE_H
