#ifndef INODE_H
#define INODE_H

#include <QSize>
#include <QList>
#include <FactoredValue.h>

class INode;

namespace choreograph {
class Timeline;
}

using IntNodeFactor = FactoredValue<uint_fast32_t, double, INode>;
using DoubleNodeFactor = FactoredValue<double, double, INode>;

class QPainter;
class INode
{
public:
    virtual ~INode();

protected:
    INode(INode *parent = nullptr);

public:
    inline uint_fast32_t width() const {return m_width;}
    inline uint_fast32_t height() const {return m_height;}
    inline INode *parent_node() const {return m_parent;}

    void update_node();
    void render_node(QPainter *painter);
    void update_node_animations(choreograph::Timeline *timeline);
    void canvas_size_changed(const QSize &size);

    void add_child(INode *child);
    void replace_child(INode *to_replace, INode* replace_with);

protected:
    virtual void update();
    virtual void render(QPainter *painter) = 0;
    virtual void update_canvas_size(const QSize &size) {
        Q_UNUSED(size)
    }
    virtual void update_animation(choreograph::Timeline *timeline) {
        Q_UNUSED(timeline);
    }

protected:
    INode* m_parent = nullptr;

    QList<INode *> m_children;
    uint_fast32_t m_width = 0;
    uint_fast32_t m_height = 0;

    IntNodeFactor m_width_factor;
    IntNodeFactor m_height_factor;
};

#endif // INODE_H
