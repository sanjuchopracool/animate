#include "RootNode.h"

#include <QtGui/QOpenGLContext>
#include <QtGui/QOpenGLFunctions>

#include <Timeline.h>
#include <Phrase.hpp>

RootNode::RootNode(INode *parent)
    : INode(parent)
{
}

void RootNode::update_background_color()
{
    if (m_bg_node)
    {
        m_bg_node->update_color_set();
    }
}

void RootNode::add_background(IBackgroundNode *node)
{
    if (node)
    {
        replace_child(m_bg_node, node);
        m_bg_node = node;
    }
}

void RootNode::render(QPainter *painter)
{
    Q_UNUSED(painter)
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glClearColor(0, 0, 0, 0);
    f->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void RootNode::update_canvas_size(const QSize &size)
{
    m_width = size.width();
    m_height = size.height();
}

void RootNode::update_animation(choreograph::Timeline *timeline)
{
    using namespace choreograph;
    timeline->apply(&m_width_factor.m_factor).
            then<Hold>(1.0, 6.5);
}
