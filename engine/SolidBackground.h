#ifndef SOLIDBACKGROUND_H
#define SOLIDBACKGROUND_H

#include <IBackGroundNode.h>
#include <QColor>
#include <QRect>

class SolidBackground : public IBackgroundNode
{
public:
    enum Style {
        Margin,
        Solid,
        Gradient2Color,
        GradientAllColor
    };

    SolidBackground(Style style, INode *parent = nullptr);

protected:
    void render(QPainter *painter) override;
    void update() override;
    void update_animation(choreograph::Timeline *timeline) override;
private:
    uint_fast32_t m_margin = 0;

    IntNodeFactor m_margin_factor;
    Style m_style = Margin;
};

#endif // SOLIDBACKGROUND_H
