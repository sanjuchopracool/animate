#ifndef COLORSET_H
#define COLORSET_H

#include <QColor>
#include <QObject>

struct ColorSet
{
    Q_GADGET
    Q_PROPERTY(QColor color1 MEMBER m_color1)
    Q_PROPERTY(QColor color2 MEMBER m_color2)
    Q_PROPERTY(QColor color3 MEMBER m_color3)
    Q_PROPERTY(QColor color4 MEMBER m_color4)

public:
    QColor m_color1 = 0xd35400;
    QColor m_color2 = 0xecf0f1;
    QColor m_color3 = 0x8e44ad;
    QColor m_color4 = 0x2c3e50;
    uint_fast32_t m_bg_count = 0;
    uint_fast32_t m_text_count = 0;
};

#endif // COLORSET_H
