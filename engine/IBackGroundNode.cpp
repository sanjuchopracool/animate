#include "IBackGroundNode.h"

#include <ColorSetModel.h>

IBackgroundNode::IBackgroundNode(INode *parent)
    : INode(parent),
      m_color_set(ColorSetModel::instance()->background_color_set())
{

}

void IBackgroundNode::update_color_set()
{
    m_color_set = ColorSetModel::instance()->background_color_set();
}
