#include "ShapePatternBackground.h"

#include <QPainter>
#include <qmath.h>

#include <Timeline.h>
#include <Phrase.hpp>

namespace {
void drawBrick(QPainter *painter,
               const QRectF &particle_rect,
               const QRectF &bounding_rect,
               double angle) {
    Q_UNUSED(particle_rect)
    Q_UNUSED(angle)
    painter->drawRect(bounding_rect);
}

void drawSolidCircle(QPainter *painter,
                     const QRectF &particle_rect,
                     const QRectF &bounding_rect,
                     double angle) {
    Q_UNUSED(bounding_rect)
    Q_UNUSED(angle)
    painter->drawEllipse(particle_rect);
}

void drawTriangle(QPainter *painter,
                  const QRectF &particle_rect,
                  const QRectF &bounding_rect,
                  double angle) {
    Q_UNUSED(bounding_rect)
    QPointF top_left = particle_rect.topLeft();
    qreal height = particle_rect.height();
    qreal width = particle_rect.width();

    QPainterPath path;
    path.moveTo(0, -height/2);
    path.lineTo(width/2, height/2);
    path.lineTo(-width/2, height/2);
    path.lineTo(0, -height/2);
    painter->translate(top_left.x() + width/2, top_left.y() + height/2);
    painter->rotate(angle);
    painter->drawPath(path);
}

void drawRectangle(QPainter *painter,
                  const QRectF &particle_rect,
                  const QRectF &bounding_rect,
                  double angle) {
    Q_UNUSED(bounding_rect)
    QPointF top_left = particle_rect.topLeft();
    qreal height = particle_rect.height();
    qreal width = particle_rect.width();

    painter->translate(top_left.x() + width/2, top_left.y() + height/2);
    painter->rotate(angle);
    painter->drawRect(-width/2, -height/2, width, height);
}


using draw_function_type = void(*)(QPainter*, const QRectF&, const QRectF&, double angle);
QMap<ShapePatternBackground::ParticleType,draw_function_type> particle_draw_funcions =
{
    {ShapePatternBackground::BrickStyle, drawBrick},
    {ShapePatternBackground::SolidCircle, drawSolidCircle},
    {ShapePatternBackground::Circle, drawSolidCircle},
    {ShapePatternBackground::Triangle, drawTriangle},
    {ShapePatternBackground::Rectangle, drawRectangle},
};

bool change_particle_size(ShapePatternBackground::ParticleType type) {
    return (ShapePatternBackground::Circle == type)
            || (ShapePatternBackground::SolidCircle == type);
}
}

ShapePatternBackground::ShapePatternBackground(ParticleType type, INode *parent)
    : IBackgroundNode(parent),
      m_particle_dim_factor(m_particle_dimension, DoubleNodeFactor::Min_width_height, 0.05)
{
    set_particle_type(type);
}

void ShapePatternBackground::update()
{
    INode::update();
    m_particle_dim_factor.update_value(this);
}

void ShapePatternBackground::render(QPainter *painter)
{
    // find no of rows and columns
    bool is_height_less = m_height <= m_width;
    double vertical_spacing = 0.0;
    double horizontal_spacing = 0.0;

    double content_height = (m_height - 2*m_particle_dimension);
    if (is_height_less)
    {
        m_row_count = m_items_in_smaller_dimension;
        vertical_spacing = content_height/(m_row_count - 1);
        m_column_count = m_width/vertical_spacing;
        horizontal_spacing = (double) m_width / m_column_count;
    }
    else
    {
        m_column_count = m_items_in_smaller_dimension;
        horizontal_spacing = (double) m_width / m_column_count;
        m_row_count = content_height/horizontal_spacing;
        vertical_spacing = content_height/(m_row_count - 1);
    }

    // set pen style
    QPen pen(Qt::SolidLine);
    pen.setColor(m_color_set.m_color2);
    painter->setPen(pen);

    if (SolidCircle == m_particle_style ||
        Rectangle == m_particle_style)
    {
        painter->setPen(Qt::NoPen);
        painter->setBrush(m_color_set.m_color2);
    }

    bool is_odd_line = true;
    double horizontal_offset = 0;
    double painter_width = painter->pen().widthF();
    double rotation_angle = 0;
    for (uint_fast32_t i = 0; i < m_row_count; ++i)
    {
        painter->save();
        // translate to y cordinate and zero x value
        painter->translate(0, m_particle_dimension + i*vertical_spacing);
        double particle_animating_dimension = m_circle_radius_factor*m_particle_dimension;
        for (uint_fast32_t j =0; j <= m_column_count; ++j)
        {
            if (is_odd_line)
            {
                horizontal_offset = (m_particle_offset_factor - 0.5)*horizontal_spacing;
                rotation_angle = m_rotation_angle;
            }
            else
            {
                horizontal_offset = (- m_particle_offset_factor)*horizontal_spacing;
                rotation_angle = -1*m_rotation_angle;
            }

            double top_left_x = j*horizontal_spacing + painter_width + horizontal_offset;
            QRect particle_rect(top_left_x,
                       -particle_animating_dimension/2,
                       particle_animating_dimension,
                       particle_animating_dimension);

            QRect bounding_rect(top_left_x,
                                -vertical_spacing/2,
                                horizontal_spacing,
                                vertical_spacing);

            painter->save();
            rotation_angle +=  m_rotation_offset*j;
            particle_draw_funcions[m_particle_style](painter,
                    particle_rect,
                    bounding_rect,
                    rotation_angle);

            painter->restore();
        }
        painter->restore();
        is_odd_line = not is_odd_line;
    }

    //draw background
    painter->setCompositionMode(QPainter::CompositionMode_DestinationOver);
    painter->fillRect(0, 0, m_width, m_height, m_color_set.m_color1);
}

void ShapePatternBackground::update_canvas_size(const QSize &size)
{
    Q_UNUSED(size);
    m_canvas_size_changed = true;
}

void ShapePatternBackground::update_animation(choreograph::Timeline *timeline)
{
    using namespace choreograph;
//    if (Triangle != m_particle_style)
    {
        timeline->apply(&m_particle_offset_factor).
                then<RampTo>(0.5, 1.5).
                then<Hold>(0.5, 3.0).
                then<RampTo>(0.0, 1.5);
    }

//    if(Triangle == m_particle_style)
    {
        timeline->apply(&m_rotation_angle).
                then<RampTo>(200, 6);
    }

//    if (change_particle_size(m_particle_style))
    {
        timeline->apply(&m_circle_radius_factor).
                then<RampTo>(0.5, 1).
                then<Hold>(0.5, 4.0).
                then<RampTo>(0.0, 1);
    }
}
