#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include <common.h>

#include <ExportSetting.h>
#include <PreviewManager.h>
#include <PreviewItem.h>
#include <ColorSetModel.h>
#include <BackgroundNodeFactory.h>


int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

    QCoreApplication::setOrganizationName("SANJUCHOPRACOOL");
    QCoreApplication::setApplicationName("ANIMATE");
    QCoreApplication::setApplicationVersion("1.0");

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;

    qmlRegisterType<PreviewItem>(package_name_chops, 1, 0, "Preview");
    qmlRegisterSingletonType( QUrl("qrc:/Style.qml"), package_name_chops,
                              1, 0, "AppStyle" );

    PreviewManager::register_types();
    ExportSetting::register_types();
    ColorSetModel::register_types();
    BackgroundNodeFactory::register_types();


    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
