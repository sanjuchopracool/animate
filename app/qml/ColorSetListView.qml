import QtQuick 2.0
import chops.animate 1.0

ListView {
    id: root
    height: 60

    orientation: ListView.Horizontal
    layoutDirection: Qt.LeftToRight
    model: ColorSetModel

    delegate: Rectangle {
        id: delegate
        property bool selected: index == currentIndex
        height: parent.height
        width: height
        border.width: selected ? 1 : 0
        color: color1
        border.color: AppStyle.border_color

        Column {
            id: inner_rect
            visible: parent.selected
            anchors.fill: parent
            anchors.margins: parent.border.width
            anchors.topMargin: parent.height/4

            Rectangle {
                height: parent.height/3
                width: parent.width
                color: color2
            }

            Rectangle {
                height: parent.height/3
                width: parent.width
                color: color3
            }

            Rectangle {
                height: parent.height/3
                width: parent.width
                color: color4
            }
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.currentIndex = index
            }
        }
    }
    onCurrentIndexChanged: {
        ColorSetModel.background_set_index = currentIndex
    }

    Component.onCompleted: {
        currentIndex = ColorSetModel.background_set_index
    }
}
