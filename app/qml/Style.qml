pragma Singleton
import QtQuick 2.0

QtObject {
    property string background_color: "#303030"
    property string border_color: "#000000"
}
