import QtQuick 2.0
import QtQuick.Controls 2.0
import chops.animate 1.0

Rectangle {
    id: root
    width: 300;
    height: 200
    color: AppStyle.background_color

    ButtonGroup {
        buttons: rowOrientation.children
        onClicked: {
            ExportSetting.picture_mode = button.mode
        }
    }

    Row {
        id: rowOrientation

        PictureModeButton {
            text: qsTr("Landscape")
            mode: ExportSetting.Landscape;
        }

        PictureModeButton {
            text: qsTr("Portrait")
            mode: ExportSetting.Portrait;
        }

        PictureModeButton {
            text: qsTr("Square")
            mode: ExportSetting.Square;
        }
    }

    Button {
        id: okButton
        text: qsTr("OK")
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10

        onClicked: {
            root.visible = false
        }
    }
}
