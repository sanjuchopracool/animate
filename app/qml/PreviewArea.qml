import QtQuick 2.11
import chops.animate 1.0

Rectangle {
    id: root
    color: AppStyle.background_color

    Preview{
        anchors.centerIn: parent
        width: PreviewManager.width
        height: PreviewManager.height

        Image {
            id: exportSettingButton
            source: "qrc:/setting.png"
            anchors.margins: 10
            anchors.bottom: parent.bottom
            anchors.right: parent.right

            MouseArea {
                id: buttonClickArea
                anchors.fill: parent

                onClicked: {
                    setting.visible = true
                }
            }
        }
    }

    Binding {
        target: PreviewManager
        property: "parent_width"
        value: root.width
    }

    Binding {
        target: PreviewManager
        property: "parent_height"
        value: root.height
    }

    ExportSettingDialog {
        id: setting
        anchors.centerIn: parent
        visible: false
    }
}
