import QtQuick 2.0
import QtQuick.Window 2.11
import chops.animate 1.0

Window {
    id: root
    visible: true
    width: 800
    height: 600
    color: AppStyle.background_color

    PreviewArea {
        id: preview_area
        anchors.top: parent.top
        anchors.bottom: backgrounds_list_view.top
        anchors.left: parent.left
        anchors.right: parent.right
    }

    BackgroundsListView {
        id: backgrounds_list_view
        anchors.bottom: color_set_view.top
        anchors.left: parent.left
        anchors.right: parent.right
        model: BackgroundNodeModel
        onCurrentIndexChanged: {
            BackgroundNodeModel.background_style_changed(currentIndex)
        }
    }

    ColorSetListView {
        id: color_set_view
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
    }
}
