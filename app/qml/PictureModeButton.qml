import QtQuick 2.0
import QtQuick.Controls 2.0
import chops.animate 1.0

Button {
    property int mode: 0
    checkable: true

    Component.onCompleted: {
        checked = mode === ExportSetting.picture_mode
    }
}
