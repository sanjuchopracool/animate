import QtQuick 2.0

ListView {
    id: root
    height: 50

    orientation: ListView.Horizontal
    layoutDirection: Qt.LeftToRight

    delegate: Rectangle {
        id: delegate

        property bool selected: index == currentIndex
        width: label.implicitWidth + 20
        height: parent.height
        border.width: selected ? 4 : 2
        border.color: selected ? "blue" : "grey"
        color: "white"

        Text {
            id: label
            anchors.centerIn: parent
            text: display
        }

        MouseArea {
            anchors.fill: parent
            onClicked: {
                root.currentIndex = index
            }
        }
    }
}
